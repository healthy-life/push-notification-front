import { all, AllEffect, TakeEffect, CallEffect } from "redux-saga/effects";

export default function* rootSaga(): Generator<
  AllEffect<Generator<TakeEffect | CallEffect>>
> {
  yield all([]);
}
