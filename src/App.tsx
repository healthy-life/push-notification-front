import React from "react";
import { Provider } from "react-redux";

import store from "./store";

const App = () => (
  <Provider store={store}>
    <div className="App">
      <header className="App-header">
        <h1>Test text</h1>
      </header>
    </div>
  </Provider>
);

export default App;
